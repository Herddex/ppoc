package com.ubb.exception;

public class IncorrectOperandCountException extends RuntimeException {
    public IncorrectOperandCountException(String message) {
        super(message);
    }
}
