package com.ubb;

import com.ubb.exception.InvalidCommandException;
import com.ubb.exception.UnknownOperationException;
import com.ubb.operationExecutor.DoubleOperationExecutor;
import com.ubb.operationExecutor.LongOperationExecutor;
import com.ubb.operationExecutor.OperationExecutor;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class CommandLineCalculator {
    private static final LongOperationExecutor LONG_OPERATION_EXECUTOR = new LongOperationExecutor();
    private static final DoubleOperationExecutor doubleOperationExecutor = new DoubleOperationExecutor();
    private static OperationExecutor operationExecutor = LONG_OPERATION_EXECUTOR;
    public static void run()
    {
        printMenu();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print(">>>");

            String command = scanner.nextLine();
            List<String> tokens = Arrays.stream(command.split(" "))
                    .filter(token -> !token.isBlank())
                    .collect(Collectors.toList());

            if (tokens.isEmpty())
                continue;

            if ("exit".equals(tokens.get(0)))
                return;

            try {
                handleCommand(tokens);
            } catch (Exception exception) {
                System.out.println("Error: " + exception.getMessage());
            }
        }
    }

    private static void handleCommand(List<String> tokens) {
        if ("set".equals(tokens.get(0))) {
            if (tokens.size() == 3 && tokens.get(1).equals("mode")) {
                if (tokens.get(2).equals("integer")) {
                    operationExecutor = LONG_OPERATION_EXECUTOR;
                } else if (tokens.get(2).equals("real")) {
                    operationExecutor = doubleOperationExecutor;
                } else {
                    throw new UnknownOperationException("Mode not supported");
                }
            } else throw new InvalidCommandException("Mode setting command is invalid");
        } else
            System.out.println(operationExecutor.executeOperation(tokens));
    }

    private static void printMenu() {
        System.out.println("Command Line Calculator");
        System.out.println("-----------------------");
        System.out.println("You can always change the mode (integer by default) with:");
        System.out.println("set mode [integer|real]");
        System.out.println("------------------------");
        System.out.println("Type 'exit' to exit.");
        System.out.println("------------------------");
        System.out.println("Available operations: +, -, /, *, min, max, sqrt,");
        System.out.println("Syntax: [operation] [operand1] [operand2]");
    }
}
