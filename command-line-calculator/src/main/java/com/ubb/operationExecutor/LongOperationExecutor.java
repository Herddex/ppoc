package com.ubb.operationExecutor;

public class LongOperationExecutor extends OperationExecutor {

    @Override
    public String addOperation(String operand1, String operand2) {
        return Long.toString(Long.parseLong(operand1) + Long.parseLong(operand2));
    }

    @Override
    public String subtractOperation(String operand1, String operand2) {
        return Long.toString(Long.parseLong(operand1) - Long.parseLong(operand2));
    }

    @Override
    public String multiplyOperation(String operand1, String operand2) {
        return Long.toString(Long.parseLong(operand1) * Long.parseLong(operand2));
    }

    @Override
    public String divideOperation(String operand1, String operand2) {
        return Long.toString(Long.parseLong(operand1) / Long.parseLong(operand2));
    }

    @Override
    public String minOperation(String operand1, String operand2) {
        return Long.toString(Long.min(Long.parseLong(operand1), Long.parseLong(operand2)));
    }

    @Override
    public String maxOperation(String operand1, String operand2) {
        return Long.toString(Long.max(Long.parseLong(operand1), Long.parseLong(operand2)));
    }

    @Override
    public String sqrtOperation(String operand) {
        long num = Long.parseLong(operand);
        if (num < 0) {
            throw new ArithmeticException("Square root operation not accepted for negative numbers");
        }

        long lowerBound = 0, higherBound = num;
        while (lowerBound < higherBound) {
            long middle = (lowerBound + higherBound + 1) / 2;
            if (middle * middle > num) {
                higherBound = middle - 1;
            }
            else if (middle * middle < num) {
                lowerBound = middle;
            }
            else {
                return Long.toString(middle);
            }
        }

        return Long.toString(lowerBound);
    }
}
