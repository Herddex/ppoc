package com.ubb;


import com.ubb.operationExecutor.LongOperationExecutor;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class LongOperationExecutorTest
{
    LongOperationExecutor longOperationExecutor = new LongOperationExecutor();

    @Test
    public void add()
    {
        assertThat(longOperationExecutor.addOperation("3", "5"), is("8"));
    }

    @Test
    public void sub()
    {
        assertThat(longOperationExecutor.subtractOperation("5", "-3"), is("8"));
    }

    @Test
    public void mul()
    {
        assertThat(longOperationExecutor.multiplyOperation("3", "-4"), is("-12"));
    }

    @Test
    public void div()
    {
        assertThat(longOperationExecutor.divideOperation("13", "-3"), is("-4"));
    }

    @Test
    public void sqrt()
    {
        assertThat(longOperationExecutor.sqrtOperation("7"), is("2"));
    }
}
