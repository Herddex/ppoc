package com.ubb;


import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class LambdasAndStreamsTest
{
    private final List<BigDecimal> bigDecimals;

    public LambdasAndStreamsTest() {
        Random random = new Random(1234L);
        bigDecimals = LambdasAndStreams.generateRandomBigDecimalList(random, 20, 100, 50);
    }

    @Test
    public void serialization_test() throws IOException {
        String pathToSerializedBigDecimalsFile = "serializedBigDecimals_test.bin";
        LambdasAndStreams.serializeBigDecimals(bigDecimals, pathToSerializedBigDecimalsFile);
        List<BigDecimal> deserializedBigDecimals =
                LambdasAndStreams.deserializeBigDecimals(pathToSerializedBigDecimalsFile);
        assertThat(deserializedBigDecimals, is(bigDecimals));
    }

    @Test
    public void bigDecimalSum_test() {
        List<BigDecimal> terms = List.of(
                new BigDecimal("3124236.45"),
                new BigDecimal("0.25"),
                new BigDecimal("100.3")
        );
        assertThat(LambdasAndStreams.bigDecimalSum(terms), is(new BigDecimal("3124337.00")));
    }

    @Test
    public void bigDecimalAverage_test() {
        List<BigDecimal> terms = List.of(
                new BigDecimal("3124236.45"),
                new BigDecimal("0.25"),
                new BigDecimal("100.3")
        );
        assertThat(LambdasAndStreams.bigDecimalAverage(terms), is(new BigDecimal("1041445.66")));
    }

    @Test
    public void topTenPercent_test() {
        List<BigDecimal> bigDecimals = Stream.of(
                3, 2, 1, 7, 19, 20, 5, 10, 11, 12, 13, 6, 8, 4, 18, 9, 14, 15, 16, 17)
                .map(BigDecimal::new).toList();
        List<BigDecimal> top10Percent = LambdasAndStreams.topTenPercent(bigDecimals);
        assertThat(top10Percent,
                is(List.of(new BigDecimal(20), new BigDecimal(19))));
    }
}
