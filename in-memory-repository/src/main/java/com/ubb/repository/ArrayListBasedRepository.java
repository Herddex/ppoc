package com.ubb.repository;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private final List<T> list;

    public ArrayListBasedRepository() {
        list = new ArrayList<>();
    }

    @Override
    public void add(T newValue) {
        list.add(newValue);
    }

    @Override
    public boolean contains(T value) {
        return list.contains(value);
    }

    @Override
    public void remove(T value) {
        list.remove(value);
    }
}
