package com.ubb.operationExecutor;

import com.ubb.exception.IncorrectOperandCountException;
import com.ubb.exception.InvalidNumberException;
import com.ubb.exception.UnknownOperationException;

import java.util.List;

public abstract class OperationExecutor {
    public abstract String addOperation(String operand1, String operand2);
    public abstract String subtractOperation(String operand1, String operand2);
    public abstract String multiplyOperation(String operand1, String operand2);
    public abstract String divideOperation(String operand1, String operand2);
    public abstract String minOperation(String operand1, String operand2);
    public abstract String maxOperation(String operand1, String operand2);
    public abstract String sqrtOperation(String operand1);

    public String executeOperation(List<String> operationCommandTokens) {
        String operation = operationCommandTokens.get(0);
        try {
            switch (operation) {
                case "+":
                    return addOperation(operationCommandTokens.get(1), operationCommandTokens.get(2));
                case "-":
                    return subtractOperation(operationCommandTokens.get(1), operationCommandTokens.get(2));
                case "*":
                    return multiplyOperation(operationCommandTokens.get(1), operationCommandTokens.get(2));
                case "/":
                    return divideOperation(operationCommandTokens.get(1), operationCommandTokens.get(2));
                case "min":
                    return minOperation(operationCommandTokens.get(1), operationCommandTokens.get(2));
                case "max":
                    return maxOperation(operationCommandTokens.get(1), operationCommandTokens.get(2));
                case "sqrt":
                    return sqrtOperation(operationCommandTokens.get(1));
                default:
                    throw new UnknownOperationException("Operation not supported");
            }
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            throw new IncorrectOperandCountException("Not enough operands for this operation");
        } catch (NumberFormatException numberFormatException) {
            throw new InvalidNumberException("Number not valid. " + numberFormatException.getMessage());
        }
    }
}
