package com.ubb.benchmarks;

import com.ubb.model.Order;
import com.ubb.repository.UnifiedSetBasedRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Random;

public class UnifiedSetBasedRepositoryBenchmark {
    @State(Scope.Thread)
    public static class AddState {
        UnifiedSetBasedRepository<Order> orders;
        int currentId;
        Order currentOrder;

        @Setup(Level.Iteration)
        public void doTrialSetup()
        {
            orders = new UnifiedSetBasedRepository<>();
        }

        @Setup(Level.Invocation)
        public void doInvocationSetup() {
            currentId += 1;
            currentOrder = new Order(currentId, 0, 0);
        }
    }

    @Benchmark
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    @Threads(16)
    public void addTest(AddState state) {
        state.orders.add(state.currentOrder);
    }

    @State(Scope.Benchmark)
    public static class ContainsState {
        UnifiedSetBasedRepository<Order> orders;
        Random random;
        Order randomOrder;

        @Setup(Level.Trial)
        public void doTrialSetup() {
            random = new Random();
            orders = new UnifiedSetBasedRepository<>();
            for (int i = 0; i < 1000; i++)
                orders.add(new Order(i, 0, 0));
        }

        @Setup(Level.Invocation)
        public void doInvocationSetup() {
            randomOrder = new Order(random.nextInt(1999) + 1, 0, 0);
        }
    }

    @Benchmark
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    @Threads(16)
    public boolean containsTest(ContainsState state) {
        return state.orders.contains(state.randomOrder);
    }

    @State(Scope.Thread)
    public static class RemoveState {
        UnifiedSetBasedRepository<Order> orders;
        Random random;
        Order randomOrder;

        @Setup(Level.Trial)
        public void doTrialSetup() {
            random = new Random();
        }

        @Setup(Level.Invocation)
        public void doInvocationSetup() {
            orders = new UnifiedSetBasedRepository<>();
            randomOrder = new Order(random.nextInt(1999) + 1, 0, 0);
            for (int i = 0; i < 10; i++)
                orders.add(new Order(i, 0, 0));
        }
    }

    @Benchmark
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 5, time = 1)
    @Threads(16)
    public void removeTest(RemoveState state) {
        state.orders.remove(state.randomOrder);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(UnifiedSetBasedRepositoryBenchmark.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
