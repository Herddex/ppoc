package com.ubb;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class LambdasAndStreams {
    private static BigDecimal generateRandomBigDecimal(Random random, int minDigitCount, int maxDigitCount) {
        int digitCount = random.nextInt(minDigitCount, maxDigitCount);
        return new BigDecimal(Stream.generate(() -> random.nextInt(0, 10))
                .map(Object::toString)
                .limit(digitCount)
                .reduce("", (x, y) -> x + y));
    }

    public static List<BigDecimal> generateRandomBigDecimalList(Random random, int minDigitCount, int maxDigitCount, int size) {
        return Stream.generate(() -> generateRandomBigDecimal(random, minDigitCount, maxDigitCount))
                        .limit(size)
                        .toList();
    }

    public static BigDecimal bigDecimalSum(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal bigDecimalAverage(List<BigDecimal> bigDecimals) {
        return bigDecimalSum(bigDecimals).divide(new BigDecimal(bigDecimals.size()), RoundingMode.DOWN);
    }

    public static List<BigDecimal> topTenPercent(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream()
                .sorted(Comparator.reverseOrder())
                .limit(bigDecimals.size() / 10)
                .toList();
    }

    public static void bigDecimalStreamOperations(List<BigDecimal> bigDecimals) {
        System.out.println(bigDecimals);
        System.out.println();

        System.out.println("Sum = " + bigDecimalSum(bigDecimals));

        System.out.println("Average = " + bigDecimalAverage(bigDecimals));

        System.out.println("Top 10%:");
        topTenPercent(bigDecimals).forEach(System.out::println);
    }

    public static void serializeBigDecimal(BigDecimal bigDecimal, ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(bigDecimal);
    }
    public static void serializeBigDecimals(List<BigDecimal> bigDecimals, String outputFile) throws IOException {
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(outputFile))) {
            objectOutputStream.writeInt(bigDecimals.size());
            bigDecimals.forEach(bigDecimal -> {
                try {
                    serializeBigDecimal(bigDecimal, objectOutputStream);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    public static BigDecimal deserializeBigDecimal(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        return (BigDecimal) objectInputStream.readObject();
    }

    public static List<BigDecimal> deserializeBigDecimals(String inputFilePath) throws IOException {
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(inputFilePath))) {
            int size = objectInputStream.readInt();
            return Stream.generate(() -> {
                try {
                    return deserializeBigDecimal(objectInputStream);
                } catch (IOException | ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }).limit(size).toList();
        }
    }

    public static void main(String[] args) {
        List<BigDecimal> bigDecimals = generateRandomBigDecimalList(new Random(),20, 100, 5);
        bigDecimalStreamOperations(bigDecimals);
    }
}
