package com.ubb.repository;

import com.koloboke.collect.set.hash.HashObjSets;

import java.util.Set;

public class KolobokeSetBasedRepository<T> implements InMemoryRepository<T>{
    private final Set<T> set;

    public KolobokeSetBasedRepository() {
        this.set = HashObjSets.newMutableSet();
    }

    @Override
    public void add(T newValue) {
        set.add(newValue);
    }

    @Override
    public boolean contains(T value) {
        return set.contains(value);
    }

    @Override
    public void remove(T value) {
        set.remove(value);
    }
}
