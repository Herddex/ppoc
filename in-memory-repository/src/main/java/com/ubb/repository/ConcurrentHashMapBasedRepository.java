package com.ubb.repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private final ConcurrentMap<T, Integer> map;

    public ConcurrentHashMapBasedRepository() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T newValue) {
        map.put(newValue, 0);
    }

    @Override
    public boolean contains(T value) {
        return map.containsKey(value);
    }

    @Override
    public void remove(T value) {
        map.remove(value);
    }
}
