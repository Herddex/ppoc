package com.ubb.repository;

public interface InMemoryRepository<T> {
    void add(T newValue);
    boolean contains(T value);
    void remove(T value);
}
