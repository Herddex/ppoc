package com.ubb.operationExecutor;

public class DoubleOperationExecutor extends OperationExecutor {

    @Override
    public String addOperation(String operand1, String operand2) {
        return Double.toString(Double.parseDouble(operand1) + Double.parseDouble(operand2));
    }

    @Override
    public String subtractOperation(String operand1, String operand2) {
        return Double.toString(Double.parseDouble(operand1) - Double.parseDouble(operand2));
    }

    @Override
    public String multiplyOperation(String operand1, String operand2) {
        return Double.toString(Double.parseDouble(operand1) * Double.parseDouble(operand2));
    }

    @Override
    public String divideOperation(String operand1, String operand2) {
        return Double.toString(Double.parseDouble(operand1) / Double.parseDouble(operand2));
    }

    @Override
    public String minOperation(String operand1, String operand2) {
        return Double.toString(Double.min(Double.parseDouble(operand1), Double.parseDouble(operand2)));
    }

    @Override
    public String maxOperation(String operand1, String operand2) {
        return Double.toString(Double.max(Double.parseDouble(operand1), Double.parseDouble(operand2)));
    }

    @Override
    public String sqrtOperation(String operand) {
        double value = Double.parseDouble(operand);
        if (value < 0) {
            throw new ArithmeticException("Square root operation not accepted for negative numbers");
        }

        return Double.toString(Math.sqrt(value));
    }
}
