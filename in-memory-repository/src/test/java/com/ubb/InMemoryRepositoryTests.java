package com.ubb;

import com.ubb.model.Order;
import com.ubb.repository.*;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class InMemoryRepositoryTests {
    private static final Order sampleOrder = new Order(1, 100, 5);

    private<T> void test(InMemoryRepository<T> repo, T sampleData) {
        assertThat(repo.contains(sampleData), is(false));
        repo.add(sampleData);
        assertThat(repo.contains(sampleData), is(true));
        repo.remove(sampleData);
        assertThat(repo.contains(sampleData), is(false));
    }

    private void testWithOrder(InMemoryRepository<Order> repo) {
        test(repo, sampleOrder);
    }

    @Test
    public void testArrayListBasedRepo() {
        testWithOrder(new ArrayListBasedRepository<>());
    }

    @Test
    public void testConcurrentHashMapBasedRepo() {
        testWithOrder(new ConcurrentHashMapBasedRepository<>());
    }

    @Test
    public void testHashSetBasedRepo() {
        testWithOrder(new HashSetBasedRepository<>());
    }

    @Test
    public void testTreeSetBasedRepo() {
        testWithOrder(new TreeSetBasedRepository<>());
    }

    @Test
    public void testUnifiedSetBasedRepo() {
        testWithOrder(new UnifiedSetBasedRepository<>());
    }

    @Test
    public void testKolobokeSetBasedRepo() {
        testWithOrder(new KolobokeSetBasedRepository<>());
    }
}
