package com.ubb;

import com.ubb.operationExecutor.DoubleOperationExecutor;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class DoubleOperationExecutorTest {
    DoubleOperationExecutor doubleOperationExecutor = new DoubleOperationExecutor();

    @Test
    public void add()
    {
        assertThat(doubleOperationExecutor.addOperation("3.25", "2.75"), is("6.0"));
    }

    @Test
    public void sub()
    {
        assertThat(doubleOperationExecutor.subtractOperation("3.25", "-2.75"), is("6.0"));
    }

    @Test
    public void mul()
    {
        assertThat(doubleOperationExecutor.multiplyOperation("3.5", "-4"), is("-14.0"));
    }

    @Test
    public void div()
    {
        assertThat(doubleOperationExecutor.divideOperation("9.0", "-2"), is("-4.5"));
    }

    @Test
    public void sqrt()
    {
        assertThat(doubleOperationExecutor.sqrtOperation("9.0"), is("3.0"));
    }
}
