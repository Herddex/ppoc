package com.ubb.repository;

import com.ubb.model.Order;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;

import java.util.Set;

public class UnifiedSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> orders;

    public UnifiedSetBasedRepository() {
        this.orders = new UnifiedSet<>();
    }

    @Override
    public void add(T newValue) {
        orders.add(newValue);
    }

    @Override
    public boolean contains(T value) {
        return orders.contains(value);
    }

    @Override
    public void remove(T value) {
        orders.remove(value);
    }
}
