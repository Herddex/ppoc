package com.ubb.repository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Comparable<T>> implements InMemoryRepository<T>{
    private final Set<T> set;

    public TreeSetBasedRepository() {
        set = new TreeSet<>();
    }

    @Override
    public void add(T newValue) {
        set.add(newValue);
    }

    @Override
    public boolean contains(T value) {
        return set.contains(value);
    }

    @Override
    public void remove(T value) {
        set.remove(value);
    }
}
